---
Week: 38
Content:  Project statup
Material: See links in weekly plan
Initials: MON
---

# Week 38

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* None at this time

### Learning goals
* None at this time

## Deliverables
* None at this time


## Schedule

Below is the tentative schedule, which may be changed depending on input fromthe students.

### Tuesday:

| Time | Activity |
| :---: | :--- |
| 8:15 | MON introduces the concepts |
| 9:00 |  You work |

### Wednesday:

| Time | Activity |
| :---: | :--- |
| 8:15  | You work |
| 12:30 | Presentations and discussions |


## Hands-on time

* None at this time


## Comments
* None at this time
