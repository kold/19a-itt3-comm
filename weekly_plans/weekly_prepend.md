---
title: 'ITT3 Communication protocols'
subtitle: 'Weekly plans'
authors: ['Morten Bo Nielsen \<mon@ucl.dk\>']
main_author: 'Morten Bo Nielsen'
date: \today
email: 'mon@ucl.dk'
left-header: \today
right-header: 'ITT3 Comm, weekly plans'
---


Introduction
====================

This document is a collection of weekly plans. It is based on the wekly plans in the administrative repository, and is updated automatically on change.

The sections describe the goals and programme for each week of the course
