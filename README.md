![Build Status](https://gitlab.com/eal-itt/19a-itt3-comm/badges/master/pipeline.svg)


# 19A-ITT3-com

weekly plans, resources and other relevant stuff for the project in IT technology 1. semester autumn.

public website for students:

*  [gitlab pages](https://eal-itt.gitlab.io/19a-itt3-comm/)

latest builds:
* [PDFs](https://gitlab.com/EAL-ITT/19a-itt3-comm/-/jobs/artifacts/master/browse/?job=build+pdfs)