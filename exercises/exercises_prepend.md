---
title: 'ITT3 Communication protocols'
subtitle: 'Exercises'
#authors: ['Morten Bo Nielsen \<mon@ucl.dk\>']
main_author: 'Morten Bo Nielsen'
date: \today
email: 'mon@ucl.dk'
left-header: \today
right-header: 'ITT3 Comm, exercises'
---


Introduction
====================

This document is a collection of exercises. They are associated with the weekly plans.

References

* [Weekly plans](https://eal-itt.gitlab.io/19a-itt3-comm/19A_ITT3_comm_weekly_plans.pdf)
